package pubbot

import (
	"log"
	"strings"
	"time"
	"net/http"
	"io"
	"bufio"
	"github.com/eclipse/paho.mqtt.golang"
	"net/url"
	"os"
)

type MqttBot struct {
	Client  mqtt.Client
	Sleep_t int64
	Count   int
	Retain  bool
	Input   string
}

func (b MqttBot) Run() error {
	// Connect to the MQTT broker
	if !b.Client.IsConnected() {
		if token := b.Client.Connect(); token.Wait() && token.Error() != nil {
			return token.Error()
		}
	}

	// Read the input file
	lines, err := readInputFile(b.Input)
	if err != nil {
		log.Fatal(err)
	}

	// Check that the input file is not empty
	lineCount := len(lines)
	if lineCount == 0 {
		log.Fatal(`Empty input file.`)
	}

	j := 0
	for i := 0; i < b.Count; i++ {
		// Start again at the beginning of the file when reaching EOF
		if j == lineCount {
			j = 0
		}

		// Split line into topic and message
		words := strings.Fields(lines[j])
		if len(words) < 2 {
			log.Fatal(`Bad line ` + string(j) + ` in the input file.`)
		}

		// Publish the message
		if token := b.publish(words[0], []byte(strings.Join(words[1:], ` `))); token.Wait() && token.Error() != nil {
			return token.Error()
		}
		j++
		time.Sleep(time.Duration(b.Sleep_t) * time.Millisecond)
	}
	return nil
}

func readInputFile(input string) ([]string, error) {
	if isUrl(input) {
		return urlToLines(input)
	} else {
		return fileToLines(input)
	}
}

func urlToLines(url string) ([]string, error) {
	resp, err := http.Get(url)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()
	return linesFromReader(resp.Body)
}

func fileToLines(path string) ([]string, error) {
	file, err := os.Open(path)
	if err != nil {
		return nil, err
	}
	defer file.Close()
	return linesFromReader(file)
}

func linesFromReader(r io.Reader) ([]string, error) {
	var lines []string
	scanner := bufio.NewScanner(r)
	for scanner.Scan() {
		lines = append(lines, scanner.Text())
	}
	if err := scanner.Err(); err != nil {
		return nil, err
	}

	return lines, nil
}

func isUrl(s string) bool {
	_, err := url.ParseRequestURI(s)
	return err == nil && strings.HasPrefix(s, "http")
}

func (b MqttBot) publish(topic string, text []byte) mqtt.Token {
	token := b.Client.Publish(topic, 0, false, text)
	token.Wait()
	return token
}