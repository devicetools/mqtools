FROM golang

WORKDIR /go/src/bitbucket.org/devicetools/mqtools
COPY . .
RUN go get -d -v ./...
RUN go install -v ./...
ENTRYPOINT mqtools
