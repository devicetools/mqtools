package main

import (
	MQTT "github.com/eclipse/paho.mqtt.golang"
	"github.com/urfave/cli"
	"log"
	"os"
	"fmt"
	"bitbucket.org/devicetools/mqtools/cmd/pubbot"
)

func main() {
	app := cli.NewApp()
	app.Usage = `MQTT publisher bot. Usage: pubot [options] <token>`
	app.Flags = []cli.Flag{
		cli.StringFlag{
			Name:   "host",
			Value:  "localhost",
			EnvVar: "MQT_PUB_HOST",
			Usage:  "The MQTT broker host.",
		},
		cli.IntFlag{
			Name:   "port",
			Value:  1883,
			EnvVar: "MQT_PUB_PORT",
			Usage:  "The MQTT broker port.",
		},
		cli.StringFlag{
			Name:   "sleep_t",
			Value:  "10",
			EnvVar: "MQT_SLEEP_T",
			Usage:  "Sleep time in milliseconds between publishes.",
		},
		cli.StringFlag{
			Name:   "count",
			Value:  "1",
			EnvVar: "MQT_PUB_COUNT",
			Usage:  "Amount of messages to send.",
		},
		cli.StringFlag{
			Name:   "uid",
			Value:  "bot",
			EnvVar: "MQT_PUB_UID",
			Usage:  "The client ID.",
		},
		cli.StringFlag{
			Name:   "username",
			EnvVar: "MQT_PUB_USERNAME",
			Usage:  "The MQTT username.",
		},
		cli.BoolFlag{
			Name:   "retain",
			EnvVar: "MQT_PUB_RETAIN",
			Usage:  "Add the retain flag to the messages.",
		},
		cli.StringFlag{
			Name:   "input",
			EnvVar: "MQT_INPUT_FILE_URL",
			Usage:  "The input file url.",
		},
	}

	//create a ClientOptions struct setting the broker address, clientid, turn
	//off trace output and set the default message handler
	app.Action = func(c *cli.Context) error {
		brokerUrl := fmt.Sprintf("tcp://%s:%d", c.String("host"), c.Int("port"))
		opts := MQTT.NewClientOptions().AddBroker(brokerUrl)
		opts.SetUsername(c.String("username"))
		//create and start a client using the above ClientOptions
		opts.SetClientID(c.String("uid"))
		bot := pubbot.MqttBot{
			Client:  MQTT.NewClient(opts),
			Sleep_t: c.Int64("sleep_t"),
			Count:   c.Int("count"),
			Retain:  c.Bool("retain"),
			Input:   c.String("input"),
		}
		return bot.Run()
	}
	err := app.Run(os.Args)
	if err != nil {
		log.Fatal(err)
	}
}
