# MQTOOLS

MQTT tools

# How to install

`docker build -t mqtools:local .`

`cp mqtools.env.example mqtools.env`

Adapt mqtools.env for your needs, rename it to mqtools.env and then:

`./run.sh`
